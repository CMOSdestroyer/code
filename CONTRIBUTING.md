# Contribute to CMOSdestroyer's code

This is a reminder of some useful commands to collaborate on this project.

### Get a copy of the code

1. If you don't have an existing SSH key pair, [generate an SSH key pair](https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair) and add the public SSH key to your GitLab account.

2. Clone the repository via `SSH`

    ```bash
    git clone git@gitlab.com:CMOSdestroyer/code.git
    ```

### Before you start working on a new feature

1. Create an issue (so we can track progress and comment on the feature).

2. Create a new branch with a name starting with issue number (`#0-feature-name` in the example) based on the latest commit of `main`.

    ```bash
    git checkout main
    git pull
    git checkout -b #0-feature-name
    ```

### Integrate change from `main`

Sometime, new code is added to `main` and you want to integrate thoses changes into `yourBranch`

```bash
git checkout yourBranch
git merge main
```

### Commit your change and send them to GitLab repository

1. Before making a commit, you should check what it will change so you don't commit stuff you don't want.

    ```bash
    git diff          //To check change before file are added
    git add <file>
    git diff --cached //To check change in the next commit
    git reset <file>  //To undo git add
    ```

2. Then commit with a one-line messages for a small change, or, for bigger changes, something like this:

    ```bash
    git commit -m "#0 A brief summary of the commit
     
    A paragraph describing: 
    - what changed; and
    - its impact."
    ```
    You can add a reference to issue number (`#0` in the example) anywhere in a commit message.

3. Then, you should push your changes to GitHub.

    ```bash
    git push
    ```

### Merge changes to `main`

1. When you are ready to merge the feature to main, create a [GitLab Merge Request](https://gitlab.com/CMOSdestroyer/code/-/merge_requests/new).

    - **Source branch** should be your feature branch
    - **Target branch** should be `main`

2. Edit merge request

    - Ensure issue number is in the title of the merge request (`#0 Add feature` for example)
    - Write a description of what you are changing
    - Assign yourself to the merge request
    - Add one reviewer
