# code

Code de l'équipe CMOSdestroyer pour le projet de la session 4 du baccalauréat en génie informatique de l'Université de Sherbrooke à l'hiver 2022.

L'équipe souhaite réaliser un pipeline graphique 2D inspiré des consoles des années 1980 pour afficher un jeu vidéo de style Bomberman.

## Installation

TODO: Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

TODO: Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Contributing

You want to make changes to this project? See [CONTRIBUTING.md](CONTRIBUTING.md) where there is some useful git commands and some instructions to easily make merge requests.

## File structure

- `common/` (Usefuss files used when we start a new project)
- `src/` (Where project will be)
  - `src/Atelier2` (A reference of graphic pipeline)
  - `src/bomberman` (Graphic pipeline for bomberman executed in `FPGA`)
  - `src/bomberman-logic` (Code for bomberman logic executed in `ARM` processor)
- `vivado-boards-master/` (Vivado Board Files for Digilent FPGA Boards)
- `vivado-library/` (Digilent Vivado library)
