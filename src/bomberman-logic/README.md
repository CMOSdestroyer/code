# GIF402 - Code de départ

## Introduction

Ce code est le point de départ du simulateur pour votre pipeline graphique.
Il représente donc comment votre pipeline devra se comporter à mi-session.
Vous n'avez pas à modifier ce code avant la deuxième moitié de la session,
mais vous pouvez très certainement commencer à l'étudier avant.

## Compilation sur PC

Pour compiler le projet en dehors de la carte Zybo, vous devez avoir les
éléments suivants :

 - CMake (au moins version 3.12)
 - [SDL2](https://www.libsdl.org)
 - Un compilateur C++ (gcc ou clang)

Assurez-vous d'avoir ces éléments déjà installés. L'approche exacte
dépend de votre distribution Linux, mais sous Ubuntu par exemple on
peut installer SDL2 avec "apt install libsdl2-dev" (c'est important
de choisir la version "-dev").
Pour compiler à la main sous Linux, macOS ou WSL, on vous suggère ces
étapes à partir du dossier du projet :

    mkdir build
    cd build
    cmake ../
    make

Vous pourrez ensuite lancer vcp_sdl ("./vcp_sdl") qui ouvrira une
fenêtre graphique montrant le résultat du pipeline.

Si vous utilisez VS Code, on vous suggère d'installer l'extension CMake.
Vous pourrez ensuite utiliser ses méthodes "Build" et "Debug" plus 
facilement.

## Contenu

### vcp.hpp et vcp.cpp

Ceci est le code du pipeline à proprement dit.
Il a été écrit pour ne dépendre que de la librairie standard C++ et
peut donc être porté relativement facilement vers d'autres plateformes.
Le rendu se fait entièrement en mémoire.

### vcp_test.cpp

Ceci est un simple exécutable de test pour vérifier quelques éléments
du pipeline sans rendu graphique.
Vous pouvez modifier son contenu à votre guise.

### vcp_sdl.cpp

Ceci est un exécutable qui permet d'afficher le résultat du pipeline en
fonctions des données trouvées dans vcp_data.hpp.
Il implémente également une boucle de rafraîchissement qui produit une
animation simple avec une tuile d'arrière-plan changeante et un acteur
en déplacement.
Vous êtes encouragés à modifier son contenu, il peut servir de prototype
pour le développement de la logique de votre jeu.

### vcp_data.hpp

Ce fichier représente les données statiques de votre jeu pour alimenter
le pipeline (tuiles d'arrière-plan, palette, ...).
Il contient quelques tuiles d'exemples et une palette de base.
C'est en quelque sorte la cartouche ou ROM de votre jeu. 
Vous pouvez le modifier à votre guise, tout en gardant en tête que son
contenu sera intégré à votre projet fort probablement sous une autre 
forme. 

## Configuration Docker

La configuration Docker et VS Code a surtout été développée pour tester 
la compilation sous Linux automatiquement.
Elle n'a pas été pensée pour être utilisée avec l'application SDL.
Libre à vous de l'adapter à vos besoins si vous souhaitez l'utiliser.
