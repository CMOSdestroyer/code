// GIF402 H22 - Pipeline pour console de jeux vidéos
// Fichier d'entête de départ avec les définitions courantes pour les types de
// données utilisés.
// 
// L'usage de termes anglais dans les noms de types ou fonctions est simplement
// par harmonie avec le reste du langage C++.
//
// François Ferland - Automne 2021


#ifndef VCP
#define VCP

#include <cstdint>
#include <cstddef>
#include <array>

namespace vcp
{
    //  Constantes paramétrisant l'ensemble du pipeline.
    
    /// Type utilisé pour stocker les couleurs brutes.
    /// Dans le simulateur, nous utilisons un entier à 32 bits pour des
    /// questions d'alignement en mémoire.
    /// L'ordre des composantes est RGBX, où R est l'octet le plus
    /// significatif et X est ignoré.
    /// Voir la fonction RGBasRawColor pour comprendre la composition.
    using RawColor = uint32_t;

    /// Type utilisé comme identifiant de couleur dans la palette.
    /// Le même type est utilisé pour les couleurs des pixels des tuiles
    /// et sprites.
    using Color    = uint8_t;

    /// Nombre de couleurs dans la palette.
    /// Note: on cherche le nombre de couleurs qui peuvent être représentées
    /// par le type utilisé (donc nombre d'octets * 256)
    const size_t   PALETTE_COUNT    = sizeof(Color) * 256;
    /// Taille de la palette de couleurs (en mémoire)
    const size_t   PALETTE_SIZE     = PALETTE_COUNT * sizeof(RawColor);

    /// Type d'array pour stocker une palette de couleurs complète.
    using Palette  = std::array<RawColor, PALETTE_COUNT>;

    /// Largeur de l'image de sortie
    const uint16_t SCREEN_WIDTH     = 256;
    /// Hauteur de l'image de sortie
    const uint16_t SCREEN_HEIGHT    = 224;

    using TileID = uint8_t;
    /// Taille d'une tuile (on suppose qu'elles sont toujours carrées)
    const uint16_t TILE_SIZE        =  16;
    /// Nombre de tuiles disponibles.
    const size_t   TILE_COUNT       = sizeof(TileID) * 256;
    /// Taille en mémoire de l'ensemble des tuiles disponibles
    const size_t   TILEMAP_SIZE     = TILE_SIZE * TILE_SIZE
                                    * TILE_COUNT
                                    * sizeof(Color);
    
    /// Type d'array utilisé pour stocker les tuiles disponibles.
    using TileMap = std::array<Color, TILEMAP_SIZE>;

    /// Taille de l'arrière-plan en nombre de tuiles (on suppose qu'il est
    /// toujours carré)
    const uint16_t BG_WIDTH         = 32;
    const uint16_t BG_HEIGHT        = BG_WIDTH;

    /// Taille en mémoire de l'arrière-plan (on suppose toujours un octet
    /// par identificateur de tuile.)
    const size_t   BG_SIZE          = BG_WIDTH * BG_WIDTH; 

    /// Coordonnées maximales dans le référentiel de l'arrière-plan (en pixels)
    const uint16_t BG_MAX_X         = BG_WIDTH * TILE_SIZE;
    const uint16_t BG_MAX_Y         = BG_HEIGHT * TILE_SIZE;

    /// Type de tableau utilisé pour l'arrière-plan
    using Background = std::array<TileID, BG_SIZE>;

    /// Identifiant des acteurs (sprites) - vient de la même liste des
    /// des tuiles. NOTE : la tuile 0 indique une tuile désactivée.
    using SpriteID = uint8_t;
                                    
    /// \brief Représentation d'une sprite à traiter.
    ///
    /// Le pipeline peut traiter un nombre fini de sprites à la fois.
    /// Celles-ci peuvent avoir des positions arbitraires dans l'écran.
    /// Elles ne sont pas contraintes au limites des tuiles.
    /// La position x et y de chaque sprite est donc une coordonnées en 
    /// pixels.
    ///
    /// Le type de sprite (id) fait référence à la l'image à utiliser.
    struct Sprite
    {
        SpriteID id;
        uint16_t x;
        uint16_t y;
    };

    /// \brief Représente l'état complet et mutable du pipeline graphique.
    ///
    /// Cette struct représente l'état complet affectant le comportement du 
    /// pipeline graphique.
    /// Ceci inclus donc toute la mémoire des tuiles et sprites, en plus des 
    /// registres de contrôle qui affectent la sortie du pipeline.
    ///
    /// Notez que ceci n'inclut pas les variables intra-image utilisé par le
    /// pipeline. 
    /// De plus, le comportement du pipeline ne peut pas être modifié depuis
    /// l'extérieur pendant qu'il prépare une image.
    /// Modifier l'état de cette structure pendant la génération de l'image
    /// résulterait en un comportement indéfini.
    ///
    struct State
    {
        /// Palette de couleurs.
        Palette palette;

        /// Ensemble des tuiles disponibles. 
        TileMap tiles; 

        /// Carte de l'arrière-plan (toujours carré).
        std::array<uint8_t, BG_SIZE> background;

        /// Liste des sprites traitées
        std::array<Sprite, 16> sprites;

        /// Décalage en X de l'arrière-plan.
        int16_t bg_offset_x;

        /// Décalage en Y de l'arrière-plan.
        int16_t bg_offset_y;

    };

    /// \brief Retourne la taille nécessaire pour stocker l'image de sortie
    ///        du pipeline.
    ///
    /// Toujours constant, puisque cette taille ne dépend que de paramètres
    /// statiques.
    ///
    static const size_t getImageSize()
    {
        return SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(RawColor);
    }

    /// \brief Convertit un tuple (r,g,b) en une couleur sur un entier.
    ///
    /// Le format de sortie est RRGGBB00.
    /// La composante R se retrouve à l'octet le plus significatif, l'octet
    /// le moins significatif se retrouve égal à 0x0.
    static RawColor RGBAsRawColor(uint8_t r, uint8_t g, uint8_t b)
    {
        return ((r << 24) | (g << 16) | (b << 8)) & (0xFFFFFF00);
    }

    /// \brief Fait le rendu d'une image en fonction de l'état du pipeline
    ///        donné.
    ///
    /// Fonction idempotente : Produit toujours le même résultat avec la 
    /// même entrée (state).
    ///
    /// Le format de sortie est toujours un tampon de cette taille :
    ///   SCREEN_WIDTH * SCREEN_HEIGHT * 4
    /// Le format de couleur est toujours RGBA (voir RawColor) où chaque
    /// canal est de 8 bits et le canal A est toujours 0xFF.
    /// ATTENTION ! Aucune vérification n'est faite pour s'assurer que
    /// le pointeur vers le tampon est valide et accessible pour toute la
    /// taille de l'image de sortie.
    /// Voir la fonction getImageSize() pour obtenir la taille du tampon
    /// souhaitée selon les paramètres fixes du pipeline.
    ///
    /// \param state État du pipeline à utiliser.
    /// \param image Pointeur vers le tampon pour l'image de sortie.
    ///
    void drawImage(const State& state, uint8_t* image);
}

#endif // VCP_HPP


