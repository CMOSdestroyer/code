// GIF402 H22 - Pipeline pour console de jeux vidéos
// Tests des fonctions de base (sans affichage).
// 
// L'usage de termes anglais dans les noms de types ou fonctions est simplement
// par harmonie avec le reste du langage C++.
//
// François Ferland - Automne 2021

#include "vcp.hpp"
#include <cstdio>

bool test_rgbconv()
{
    using namespace vcp;
    uint32_t expected = 0x11223300;
    RawColor res = RGBAsRawColor(0x11, 0x22, 0x33);
    if (res != expected) {
        printf("Error, RGBAsColor returned %ux, expected %ux", res, expected);
        return false;
    }

    return true;
}

int main(int argc, char** argv)
{
    return test_rgbconv();

}