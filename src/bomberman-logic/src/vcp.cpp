// GIF402 H22 - Pipeline pour console de jeux vidéos
// Implémentation de base des fonctions du simulateur.
// 
// L'usage de termes anglais dans les noms de types ou fonctions est simplement
// par harmonie avec le reste du langage C++.
//
// François Ferland - Automne 2021

#include "vcp.hpp"

using namespace vcp;

void vcp::drawImage(const State& state, uint8_t* image)
{
    // NOTE: Pas vraiment optimisé.
    // Normalement, on ferait au minimum une copie de chaque rangée de pixels
    // des tuiles plutôt que de passer pixel par pixel.
    // Or, faire une boucle imbriquée ainsi se rapproche de la logique combinatoire
    // qui devra être implémentée sur le FPGA.
    // Note : i représente l'indice de la rangée (donc coordonnée en Y) et j la
    // colonne (coordonnée en X).
    for (int i = 0; i < SCREEN_HEIGHT; ++i) {
        for (int j = 0; j < SCREEN_WIDTH; ++j) {
            // Coordonnées dans le référentiel de l'arrière-plan (donc avec décalage).
            // NOTE: le double modulo permet l'effet mirroir avec un décalage négatif.
            // Ex: -3 % 5 = -2 (mais on veut 2), donc (-3 % 5) + 5 = 7, 7 % 5 = 2.
            int bg_i = (((i + state.bg_offset_y) % BG_MAX_Y) + BG_MAX_Y) % BG_MAX_Y;
            int bg_j = (((j + state.bg_offset_x) % BG_MAX_X) + BG_MAX_X) % BG_MAX_X;

            // Coordonnées de la tuile (1..31 dans chaque dimension) sur la carte de 
            // l'arrière-plan
            int bg_x = bg_j / TILE_SIZE;
            int bg_y = bg_i / TILE_SIZE; 
            // Coordonnées en pixels à l'intérieur de la tuile
            int u = bg_j % TILE_SIZE;
            int v = bg_i % TILE_SIZE;

            // Identifiant de la tuile
            TileID tile = state.background[bg_y * BG_WIDTH + bg_x];
            // Couleur (dans la palette) du pixel de la tuile 
            Color c = state.tiles[tile*TILE_SIZE*TILE_SIZE + v*TILE_SIZE + u];
            // Couleur (complète) correspondante dans la palette
            RawColor rc = state.palette[c];

            // On copie simplement la couleur à l'emplacement sur la texture.
            RawColor* dst = (RawColor*)(image + 
                                        (i * SCREEN_WIDTH + j) * sizeof(RawColor));
            *dst = 0x000000FF | rc; // NOTE: On force l'alpha à 1.0 en sortie.
                                    // Requis de l'API graphique qui sollicite le pipeline.

            // Rendu des acteurs (sprites)
            // On parcoure la liste des sprites:
            // Si une tuile est active (id != 0), on vérifie si elle couvre le pixel
            // en cours.
            // Si c'est le cas, on extrait la couleur. 
            // Si la couleur extraite n'est pas zéro (qui représente la transparence),
            // on l'utilise (elle écrase donc l'arrière-plan et les sprites précédentes).
            // PISTE D'OPTIMISATION : Plutôt que revérifier la liste à tous les pixels,
            // trouver la liste des sprites actives une seule fois par ligne.
            for (int s = 0; s < state.sprites.size(); ++s) {
                const Sprite& sprite = state.sprites[s];
                if (sprite.id != 0) {
                    if ( (j > sprite.x) &&
                         (j < sprite.x + TILE_SIZE) &&
                         (i > sprite.y) &&
                         (i < sprite.y + TILE_SIZE)) {
                             SpriteID si = sprite.id;
                             int su = j - sprite.x;
                             int sv = i - sprite.y;
                             Color c = state.tiles[si*TILE_SIZE*TILE_SIZE + sv*TILE_SIZE + su];
                             if (c != 0) {
                                 *dst = 0x000000FF | state.palette[c];
                             }
                         }
                }

            }
        }
    }
}