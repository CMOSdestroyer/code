// GIF402 H22 - Pipeline pour console de jeux vidéos
// Programme de test du pipeline basé sur SDL.
// 
// L'usage de termes anglais dans les noms de types ou fonctions est simplement
// par harmonie avec le reste du langage C++.
//
// François Ferland - Automne 2021

#include "vcp.hpp"
#include "vcp_data.hpp"
#include <SDL.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>

namespace {
    // Objets utilisés par SDL pour représenter la fenêtre et le système de
    // rendu.
    SDL_Window*     window_     = nullptr;
    SDL_Renderer*   renderer_   = nullptr;
    // Texture utilisée pour le rendu de l'image
    SDL_Texture*    texture_    = nullptr;

    vcp::State      state_;

    static const uint32_t LOOP_TICKS = (1000 / 60); // 1 tick SDL = 1 ms
}

/// Fonction utilitaire pour afficher sur stderr le dernier message d'erreur
/// généré par SDL.
void showSDLError()
{
    fprintf(stderr, "SDL Error: %s\n", SDL_GetError());
}

/// Initialise SDL, la librairie utilisée pour gérer l'affichage du pipeline
/// dans une fenêtre.
/// Si l'appel fonctionne, retourne true. Le résultat est une fenêtre noire de
/// la dimension configurée dans vcp.hpp.
bool initSDL()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        showSDLError();
        return false;
    }
    atexit(SDL_Quit);

    if (SDL_CreateWindowAndRenderer(vcp::SCREEN_WIDTH,
                                    vcp::SCREEN_HEIGHT,
                                    0,
                                    &window_,
                                    &renderer_) != 0) {
        showSDLError();
        return false;
    }

    texture_ = SDL_CreateTexture(renderer_,
                                 SDL_PIXELFORMAT_RGBA8888,
                                 SDL_TEXTUREACCESS_STREAMING,
                                 vcp::SCREEN_WIDTH,
                                 vcp::SCREEN_HEIGHT);

    if (texture_ == nullptr) {
        showSDLError();
        return false;
    }

    SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 0);
    SDL_RenderClear(renderer_);
    SDL_RenderPresent(renderer_);

    return true;
}

/// Initialise le pipeline avec les données par défaut.
void initVCP()
{
    state_.palette    = vcp::DefaultPalette;
    state_.tiles      = vcp::DefaultTileMap;
    state_.background = vcp::DefaultBackground;

    // Carré vert à tous les coins 8x8 pour mieux identifier les dimensions.
    for (int i = 0; i < vcp::BG_HEIGHT; i+=8) {
        for (int j = 0; j < vcp::BG_WIDTH; j+=8) {
            state_.background[vcp::BG_WIDTH*i + j] = 4;
        }
    }

    state_.sprites[0].id = 3; // Losange rouge
    state_.sprites[0].x = 4;
    state_.sprites[0].y = 14;
}

void testDraw()
{
    void* pixels; // Pointeur vers la mémoire qui contiendra l'image de sortie.
    int pitch;    // Largeur, en octets, d'une ligne de l'image
                  // (pas nécessairement la largeur X 4 bytes).

    if (SDL_LockTexture(texture_, nullptr, &pixels, &pitch) < 0) {
        showSDLError();
        exit(-1);
    }

    vcp::drawImage(state_, (uint8_t*)pixels);
    
    // On envoie le résultat à l'écran.
    SDL_UnlockTexture(texture_);
    SDL_RenderClear(renderer_);
    SDL_RenderCopy(renderer_, texture_, nullptr, nullptr);
    SDL_RenderPresent(renderer_);
}

int main(int argc, char** argv)
{
    if (!initSDL()) {
        fprintf(stderr, "SDL Init error, cannot continue.\n");
        return -1;
    }

    initVCP();

    // Boucle d'événements (souris, clavier, ...) provenant de l'OS à traiter.
    SDL_Event event;

    // Pour l'animation du losange :
    int l_d_x = 1;
    int l_d_y = 2;

    // Pour le décalage :
    int bg_d_x = 3;
    int bg_d_y = 0;

    int cycle = 0; // Compteur de cycles

    while (!(event.type == SDL_QUIT)) {
        // Temps du prochain tour de boucle (maintenant + 1/60 s):
        uint32_t next_loop_time = SDL_GetTicks() + LOOP_TICKS; 

        if ((cycle % 3) == 0) { // Une fois toutes les 3 images, ou 0.05 s
            // Animation du losange (sprite 0):
            uint16_t& x = state_.sprites[0].x;
            uint16_t& y = state_.sprites[0].y;

            x += l_d_x;
            y += l_d_y;

            if (x == 0 || x > (vcp::SCREEN_WIDTH - vcp::TILE_SIZE)) {
                l_d_x *= -1;
            }
            if (y == 0 || y > (vcp::SCREEN_HEIGHT - vcp::TILE_SIZE)) {
                l_d_y *= -1;
            }

            state_.bg_offset_x += bg_d_x;
            state_.bg_offset_y += bg_d_y;
        }

        if ((cycle % 60) == 0) { // Toutes les secondes
            // Cyclage de la tuile 5 de 0 à 3.
            state_.background[4] = (state_.background[4] + 1) % 4;
        }
            
        if ((cycle % 360) == 0) { // Toutes les 6 secondes 
            int tmp = bg_d_x;
            bg_d_x = bg_d_y;
            tmp *= -1;
            bg_d_y = tmp;
        }

        // Génération de l'image:
        testDraw();

        // Gestion des événements SDL (ignoré sauf QUIT pour l'instant):
        SDL_PollEvent(&event);

        // Calcul du temps et attente au prochain cycle (s'il y a du temps).
        uint32_t now = SDL_GetTicks();
        if (now < next_loop_time) {
            SDL_Delay(next_loop_time - now);
        }

        cycle++;
    }

    return 0;

}