#include "scaler.h"
#include "xil_printf.h"
#include "myColorRegister.h"
#include "sleep.h"

int main()
{
	configureScaler();

    int colorA = 0x000011;
    int colorB = 0x000000;
    MYCOLORREGISTER_mWriteReg(XPAR_MYCOLORREGISTER_1_S00_AXI_HIGHADDR, 4, colorB);

    while(1)
    {
    MYCOLORREGISTER_mWriteReg(XPAR_MYCOLORREGISTER_1_S00_AXI_BASEADDR, 0, colorA );
    colorA = colorA + 1024;
    sleep(1);
    }

    return 0;
}
