# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: /home/philippe/Documents/git/code/src/Atelier2/Aterlier2_system/_ide/scripts/systemdebugger_aterlier2_system_4_standalone.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source /home/philippe/Documents/git/code/src/Atelier2/Aterlier2_system/_ide/scripts/systemdebugger_aterlier2_system_4_standalone.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zybo Z7 210351AF2565A" && level==0 && jtag_device_ctx=="jsn-Zybo Z7-210351AF2565A-13722093-0"}
fpga -file /home/philippe/Documents/git/code/src/Atelier2/Aterlier2/_ide/bitstream/Atelier2_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /home/philippe/Documents/git/code/src/Atelier2/Atelier2_wrapper_3/export/Atelier2_wrapper_3/hw/Atelier2_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /home/philippe/Documents/git/code/src/Atelier2/Aterlier2/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /home/philippe/Documents/git/code/src/Atelier2/Aterlier2/Debug/Aterlier2.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
