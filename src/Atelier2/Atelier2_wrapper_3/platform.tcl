# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /home/philippe/Documents/git/code/src/Atelier2/Atelier2_wrapper_3/platform.tcl
# 
# OR launch xsct and run below command.
# source /home/philippe/Documents/git/code/src/Atelier2/Atelier2_wrapper_3/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {Atelier2_wrapper_3}\
-hw {/home/philippe/Documents/git/code/src/Atelier2/Atelier2_wrapper.xsa}\
-fsbl-target {psu_cortexa53_0} -out {/home/philippe/Documents/git/code/src/Atelier2}

platform write
domain create -name {standalone_ps7_cortexa9_0} -display-name {standalone_ps7_cortexa9_0} -os {standalone} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {empty_application}
platform generate -domains 
platform active {Atelier2_wrapper_3}
domain active {zynq_fsbl}
domain active {standalone_ps7_cortexa9_0}
platform generate -quick
platform generate
platform generate -domains standalone_ps7_cortexa9_0,zynq_fsbl 
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
